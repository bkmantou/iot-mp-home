export default defineAppConfig({
  pages: [
    'pages/login/login',
    'pages/login/register',
    'pages/index/index',
    'pages/device/index',
    'pages/device/add',
    'pages/device/detail',
    'pages/my/index',
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  }
})
