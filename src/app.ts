import { createApp } from 'vue'
import { Button, Toast,Cell, CellGroup, Icon, Tabbar,TabbarItem ,Swiper,SwiperItem ,
  Grid, GridItem,InfiniteLoading,Layout,Row,Col ,Switch,Avatar,
  Menu, MenuItem,Input,Form,FormItem,Sticky,Notify,Divider,
  OverLay, Popup,Radio,RadioGroup,Empty,Tag,Skeleton,Tabs, TabPane,ActionSheet,NoticeBar} from '@nutui/nutui-taro';

import './app.scss'
import Taro from '@tarojs/taro';

const app = createApp({
  mounted(){
    if(process.env.TARO_ENV==='weapp'){
      Taro.cloud.init({
        env: 'dev',
        traceUser:true
      })
    }
  },
  onShow (options) {},
  // 入口组件不需要实现 render 方法，即使实现了也会被 taro 所覆盖
})

let components=[ Button, Toast,Cell, CellGroup, Icon, Tabbar,TabbarItem ,Swiper,SwiperItem ,
   Grid, GridItem,InfiniteLoading,Layout,Row,Col ,Switch,Avatar, Menu, MenuItem,Input,
   Form,FormItem,Sticky,Notify,Divider, OverLay, Popup,Radio,RadioGroup,Empty,Tag,Skeleton,
   Tabs, TabPane,ActionSheet,NoticeBar];

components.forEach(c=>{
  app.use(c);
})

export default app
