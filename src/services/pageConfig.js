import Taro from "@tarojs/taro";

const pages={
    home:"/pages/index/index",
    devices:"/pages/device/index",
    my:"/pages/my/index",
    tabSwitch:function(item, index) {
        Taro.reLaunch({
            url: item.to,
        });
    }
}

export default pages;