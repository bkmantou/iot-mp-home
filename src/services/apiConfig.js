let baseUrlPrefix = ''
let authUrl=''
const env = process.env.NODE_ENV === 'development' ? 'development' : 'production'
console.log('编译环境：',process.env.NODE_ENV)
switch (env) {
  case 'development':
    baseUrlPrefix = 'http://127.0.0.1:8086';
    authUrl='http://127.0.0.1:8086/oauth2';
    break
  case 'production':
    baseUrlPrefix = 'https://springboot-wsvi-1914448-1300481250.ap-shanghai.run.tcloudbase.com';
    authUrl = 'https://springboot-wsvi-1914448-1300481250.ap-shanghai.run.tcloudbase.com/weapp-auth';
    break
}
console.log("authUrl:",authUrl)

const api = {
  baseUrl: baseUrlPrefix,
  authUrl: authUrl
//其他相关变量
}

export default api